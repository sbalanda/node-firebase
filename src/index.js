const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const { addProduct } = require('./controller/productController')
const { addCities, updateCities, getCity, getCities, getOrderByAndLimit } = require('./controller/citiesController')
const { addUser, authenticate } = require('./controller/authController')

const app = express()
const port = 5000
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) =>{
     return res.send("hola")
})

app.post('/api/signup', addUser)

app.post('/api/auth', authenticate)

app.post('/api/cities', addCities)

app.put('/api/cities', updateCities)

app.get('/api/city', getCity)

app.get('/api/cities', getCities)

app.get('/api/cities/order', getOrderByAndLimit)

app.post('/api/product', addProduct/* (req, res) =>{
    console.log("post product ",req.body)
} */)

app.listen(port,()=>{
    console.log("server puerto ",port)
})