const firebase = require('../db')
const firestore = firebase.firestore()
const admin = require('firebase-admin');

const addCities = async (req, res, next) => {
    const {
        name,
        state,
        country
    } = req.body;

    // const data = {
    //     name: 'Tokyo',
    //     state: 'JP',
    //     country: 'Japan',
    //     //dateExample: admin.firestore.Timestamp.fromDate(new Date('December 10, 2015'))
    // }
      
    const data = {
        name: name,
        state: state,
        country: country,
    }
    
    try {
        const cities = await firestore
        .collection('cities')
        .doc(state)
        .set(data);
        
        res.status(201).json(cities);
    }catch(err) {
        res.status(401).json({ error: err.message });
    }
}

const updateCities = async (req, res, next) => {
    const { document, collection, capital } = req.body;
    try {
        const cityRef = firestore.collection('cities').doc(document);
        const response = await cityRef.update({capital: capital});
        res.status(201).json(response);
    }catch(err) {
        res.status(401).json({ error: err.message });
    }
}

const getCity = async (req, res, next) => {
    const { document, collection } = req.body;
    try {
        const cityRef = firestore.collection(collection).doc(document);
        const doc = await cityRef.get();
        if (!doc.exists) {
            res.status(401).json({ error: "no existe" });
        } else {
            res.status(201).json(doc.data());
        }

    }catch(err) {
        res.status(401).json({ error: err.message });
    }
}

const getCities = async (req, res, next) => {
    const { collection } = req.body;
    try {
        const result = []
        const cityRef = await firestore.collection(collection);
        const snapshot = await cityRef.get();
        //const snapshot = await cityRef.where('capital', '==', true).get();
        snapshot.forEach(doc => {
            result.push({ id: doc.id, ...doc.data() })
        });
        res.status(201).json(result);
    }catch(err) {
        res.status(401).json({ error: err.message });
    }
}

const getOrderByAndLimit = async (req, res, next) => {
    const { collection, document, name, order, limit } = req.body;
    try {
        const result = []
        const last = await firestore
            .collection(collection)
            .orderBy(name, order)
            .limit(limit)
            .get();
        last.forEach(doc => {
            result.push({ id: doc.id, ...doc.data() })
        });
        res.status(201).json(result);
    }catch(err) {
        res.status(401).json({ error: err.message });
    }
}

module.exports = {
    addCities,
    updateCities,
    getCity,
    getCities,
    getOrderByAndLimit
}