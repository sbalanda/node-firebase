const firebase = require('../db')
const Product = require('../models/product')
const firestore = firebase.firestore()

const addProduct = async (req, res, next) => {
    console.log("addProduct ",req.body);
    try {
        const data = req.body
        const product = await firestore
        .collection('products')
        .doc()
        .set(data);
        res.status(201).json(product);
    }catch(err) {
        res.status(401).json({ error: err.message });
    }
}


module.exports = {
    addProduct
}