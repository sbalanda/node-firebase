const firebase = require('../db')
const firestore = firebase.firestore()
require("firebase/auth");

const addUser = async (req, res, next) => {
    const { email, password } = req.body;
    try {
      const user = await firebase.auth().createUserWithEmailAndPassword(email, password);
      console.log(user)
      res.status(201).json(user);
    } catch (err) {
      res.status(401).json({ error: err.message });
    }
}

const authenticate = async (req, res, next) => {
    const { email, password } = req.body;
    try {
      const user = await firebase.auth().signInWithEmailAndPassword(email, password);
      res.status(201).json(user);
    } catch (err) {
      res.status(401).json({ error: err.message });
    }
}


module.exports = {
    addUser,
    authenticate
}