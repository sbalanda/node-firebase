const firebase = require('firebase');
const config = require('./config/config')

//const credentials = require("./account.json");

const db = firebase.initializeApp(config.firebaseConfig)

module.exports = db