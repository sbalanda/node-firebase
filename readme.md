Como su propio nombre indica, las bases de datos no relacionales son las que, a diferencia de las relacionales, no tienen un identificador que sirva de relación entre un conjunto de datos y otros. Cambia el formato uno son tablas y otro un objeto.
En una base de datos no relacional una unidad de datos puede llegar a ser demasiado compleja como para plasmarlo en una tabla. Por ejemplo, en el documento JSON las relaciones al tener elementos jerárquicos, es más difícil plasmarlo en una tabla plana. 

# connect 

const firebase = require("firebase-admin");

const credentials = require("./account.json");

firebase.initializeApp({
  credential: firebase.credential.cert(credentials),
  databaseURL: "https://<yourproject>.firebaseio.com",
});

module.exports = firebase;

ir configuracion del proyecto cuentas de servicios generar nueva clave


# auth firebase
- crear proyecto en un firebase
- ir a Authentication habilitar email/password
- crear proyecto yarn init
- yarn add express firebase-admin
- tambien se utiliza parse-body para extraer la parte del cuerpo de solicitud y dotenv para tener las variables de configuracion en un env
- se crear archivo server.js con express

    const express = require('express')
    const cors = require('cors')
    const bodyParser = require('body-parser')
    const config = require('./config/config')

    const app = express()
    const port = 5000
    app.use(bodyParser.json())


    app.get('/', (req, res) =>{
        return res.send("hola")
    })

- se crea archivo config.js que utilizara las varialbes de conexion de firebase y se exporta para luego inicializar firebase en otro archivo
const dotenv = require('dotenv');
dotenv.config()

const {...} = process.env
module.exports = {firebaseConfig:{...}}

- se crear archivo para inicializar fireebase db.js
const firebase = require('firebase');
const config = require('./config/config')

const db = firebase.initializeApp(config.firebaseConfig)

module.exports = db


- se crear un controlador con dos metodos en cual reliza en create user y el signIn
const firebase = require('../db')
require("firebase/auth");

Para crear
firebase.auth().createUserWithEmailAndPassword(email, password);

Para el signIn
firebase.auth().signInWithEmailAndPassword(email, password);

se exportan ambos metodos

Dentro de server.js se llama al archivo y sus dos metodos y se crean dos ruta de tipo post

app.post('/api/signup', nombreMetodo)

app.post('/api/auth', nombreMetodo)


# La escritura en lotes puede contener hasta 500 operaciones.

    // Get a new write batch
    const batch = db.batch();

    // Set the value of 'NYC'
    const nycRef = db.collection('cities').doc('NYC');
    batch.set(nycRef, {name: 'New York City'});

    // Update the population of 'SF'
    const sfRef = db.collection('cities').doc('SF');
    batch.update(sfRef, {population: 1000000});

    // Delete the city 'LA'
    const laRef = db.collection('cities').doc('LA');
    batch.delete(laRef);

    // Commit the batch
    await batch.commit();


https://firebase.google.com/docs/admin/setup
https://dev.to/betiol/how-to-handle-authentication-on-node-js-using-firebase-5ajn
https://medium.com/@savinihemachandra/creating-rest-api-using-express-on-cloud-functions-for-firebase-53f33f22979c
https://www.youtube.com/watch?v=Ld4OGwpQ2Yk

https://firebase.google.com/docs/firestore/query-data/get-data?hl=es
https://firebase.google.com/docs/firestore/manage-data/add-data?hl=es
https://firebase.google.com/docs/firestore/query-data/queries?hl=es